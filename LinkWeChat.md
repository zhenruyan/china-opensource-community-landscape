# LinkWechat 社区
## （一）社区简介
基于人工智能的企业微信 SCRM 系统——LinkWeChat基于企业微信开放能力，不仅集成了企微基础的客户管理和后台管理功能，而且通过引流获客、客情维系、社群运营等灵活高效的客户运营模块，让客户与企业之间建立强链接关系，同时进一步通过多元化的客户营销工具，帮助企业提高客户运营效率，强化营销能力，拓展盈利空间，是企业私域流量管理与营销的综合解决方案。LinkWeChat社区非常欢迎有更多热爱技术、热爱开源的朋友加入到 LinkWeChat 的社区建设及项目贡献中来，你们为 LinkWeChat 所做的任何贡献都会被永久记录在 LinkWeChat 的贡献列表中。

## （二）发展现状
社区目前项目管理委员会（PMC）有7名核心成员，项目组织委员会有10名核心成员，开发者社区目前有活跃贡献（Contributor,Member）者数量100+，committer数量39+。社区还建立微信群、邮件列表、Gitee issues、需求收集表等方式，方便与用户深入与及时的交流，挖掘到用户与开发者的真实需求。

<img src="images/image26.png" >

## （三）治理模式
LinkWeChat的社区组织架构分为：项目管理委员会、项目组织委员会、开发者社区。其中目前项目管理委员会（PMC）主要为制定与管理相关项目规划；项目组织委员主要负责规划的具体执行；开发者社区建立了完善的Contributor -> Member -> Reviewer -> Committer -> PMC的发展路径，为了帮助热爱开源与技术的小伙伴快速加入社区建设，与LinkWeChat一起共同成长！并且还设立了SIG兴趣小组，让开发者和用户可以选择自己感兴趣的板块参与，用户和开发者更能深度的参与！
<img src="images/image27.png" >
<img src="images/image28.png" >


## （四）运营实践
为了更好让用户用起来LinkWeChat，让LinkWeChat产品贴近市场市场需求，更快速满足用户需求的，LinkWechat社区发布了[需求收集表](https://docs.qq.com/sheet/DZWxGU0JGVFRVdWZV?tab=BB08J2)，让用户真正的需求能得以落地。还积极联合其他社区多次组织线下技术交流，先后与阿里云社区，ES社区、S OFA社区，APISIX社区，神策数据等社区多次联合举办Meetup，让用户和开发者近距离贴近社区和产品提供了平台。截止目前，LinkWeChat社区成员已至近 3000 余人，企业百余家！
<img src="images/image31.png" >
<img src="images/image32.png" >
<img src="images/image29.png" >

## （四）未来规划

<img src="images/image30.png" >
