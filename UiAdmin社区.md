# UiAdmin 开源社区
## 社区简介
UiAdmin社区建于2019年，致力于推广和普及UiAdmin框架的使用。

## 治理模式
鼓励社区贡献、合作。社区的愿景是基于“共建、共享、共治”的原则，凝聚多种力量。


![UiAdmin](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-f12e1180-fce8-465f-a4cd-9f2da88ca0e6/ba0c3585-fa80-4277-9ea2-46b08a23a4bf.png)

# 安装
请参考文档

## UiAdmin(ThinkPHP6.0版本)
UiAdmin是一套零前端代码通用后台，采用前后端分离技术，数据交互采用json格式；通过后端Builder不需要一行前端代码就能构建一个vue+element的现代化后台；同时我们打造一了套兼容性的API标准，从ThinkPHP6.0、SpringBoot、.NET5开始，逐步覆盖Go、Node.jS等多语言框架。

## 特性

### 模块化
UiAdmin后台本着高内聚低耦合的原则， 模块作为UiAdmin的最小功能包可以共享 用户可以在模块市场上传下载模块

### Builder动态页面构建

UiAdmin首创自主研发了基于前后端分离的 页面自动生成技术，目前支持xyBuilderList和 xyBuilderForm，前者自动生成列表后者自动 生成表单，二者结合可以完成90%以上的 后台功能需求。

## 多平台支持

UiAdmin诞生在移动互联网后半场，面多各种 流量入口，UiAdmin将从如下方面对多个平台支持： pc端采用web方式实现，手机端将采用uni-app技术， 达到一次开发全面覆盖iOS、安卓、微信小程序、支 付宝小程序、百度小程序、头条小程序、H5，从而 节省开发者的大量精力。

### 多语言API兼容

UiAdmin后台将打造统一的后台框架体系， 后端横跨php、java、python、node、.net 等等语言，前端将支持vue、react、angular 语言，多个语言支持通过统一的API标准兼容.

## 资源
官方网站：https://uiadmin.net  
成功案例：https://uiadmin.net/case  
插件市场：https://uiadmin.net/ext  

## 解决问题
文档手册：https://uiadmin.net/docs/uiadmin1-2
交流社区：https://uiadmin.net/ask  
或者通过下面的QQ群进行提问

## 开源地址
码云仓库：https://gitee.com/uiadmin  
github：https://github.com/ijry/uiadmin 
